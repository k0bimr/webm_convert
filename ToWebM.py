import os
import subprocess
import time
import math
import sys
from Multimedia_File_Properties import VideoFileFormat

# Review
def set_tile_columns(user_filename):
    reference_width = 512
    FSW = "ffprobe -v error -show_entries stream=width -of default=noprint_wrappers=1:nokey=1 \"{0}\"".format(user_filename)
    width = int((subprocess.check_output(FSW, shell=True)).decode("utf-8"))
    tile_columns = math.floor((width / reference_width))

    return tile_columns


def set_threads(tile_columns):
    threads = 1

    if tile_columns < 5:
        threads = 2**(tile_columns + 1)
    if tile_columns >= 5:
        threads = 24

    return threads


def process_bitrate(target_file_size):
    # user input should only contain numbers and a single radix point

    # if the user enters a radix point with either nothing or 0s following then 
    # it will be typecasted to int

    # if the user enters nothing preceding the radix point, then it will assume 0 
    # and if there are non-zero numbers following the radix point, then it will 
    # be typecasted to float

    # should ensure that the user input does not equal 0 in any literal form

    radix_point_found       = False
    non_zeroes_before_radix = False
    non_zeroes_after_radix  = False
    decimal_number          = False

    for char in target_file_size:
        assert (char in "0123456789."), "Invalid input" # Ensure that each character in the user input is valid
        assert not (char == '.' and radix_point_found == True), "Invalid input" # Ensure there is only 1 radix point

        if char == '.' and radix_point_found == False: # Maybe "and radix_point_found == False" is unnecessary
            radix_point_found = True
        elif char in "123456789":
            if radix_point_found == False:
                non_zeroes_before_radix = True
            else:
                non_zeroes_after_radix = True
                decimal_number = True

    assert not (non_zeroes_before_radix == False and non_zeroes_after_radix == False), "File size cannot be 0"

    if decimal_number == True:
        target_file_size = float(target_file_size)
    else :
        target_file_size = int(target_file_size)

    return target_file_size


def process_resolution(resolution):
    assert ":" in resolution, "Non valid resolution"
    resolution = resolution.split(":")
    assert (len(resolution) == 2 and (resolution[0] != "" and resolution[1] != "")), "empty"
    for i in range(1):
        for c in resolution[i]:
            assert c in "-0123456789", "Non valid resolution"
    L = resolution[0]
    H = resolution[1]

    return L+":"+H


def process_filename_and_extension_strip(filename):
    # incoming filename could be any string, but perhaps not one containing 
    # illegal characters in the windows filesystem. {/, \, ?, :, *, ", <, >, |}

    illegal_characters = ['/', '\\', '?', ':', '*', '"', '<', '>', '|']
    PO = ""
    assert filename != "", "No filename was inputted"
    for current_char in filename:
        # assert current_char not in illegal_characters, "Illegal filename inputted"
        for illegal_char in illegal_characters:
            assert current_char != illegal_char, "Illegal filename inputted"

    RI = 0
    FRI = 0
    while(True):
        RI = filename[FRI:].find(".")
        if RI != -1:
            FRI = (FRI + RI) + 1
        else:
            FRI -= 1
            break
    if ".webm" in filename:
        PO = "1"
    PF = "{0}{1}".format(filename[:FRI], PO)

    return PF


def parse_duration(returned_duration):
    duration_list = returned_duration.split()
    maximum_duration = duration_list[0]
    for current_duration in duration_list[1:]:
        if current_duration > maximum_duration:
            maximum_duration = current_duration

    maximum_duration = float(maximum_duration)

    return maximum_duration


# returns a list of the starting position and the duration
def validate_user_clippings(start, end):
    # the start time is intended to be the value for the -ss option
    # and the end time is intended to give us a way to calculate the duration
    # for the -t option
    
    # both options require inputs of the proper Time Duration format:
    # 1.
    #   [-][HH:]MM:SS[.m...]
    #       
    #   Where:
    #       HH expresses the number of hours
    #       MM expresses the number of minutes, max of 2 digits
    #       SS expresses the number of seconds, max of 2 digits
    #       m expresses the decimal value for SS
    #
    # 2.
    #   [-]S+[.m...]
    #
    #   Where:
    #       S expresses the number of seconds   
    #       m expresses the decimal value for SS
    #
    #
    # In both expressions, '-' indicates negative duration
    #
    # It seems that only digits with the exception of ['.', '-', ':'] are allowed
    # with there being a max of (1, 1, 2) of each respectively
    #
    # If we see that there exists a colon in the user_input string, then 
    # it will be assumed that the format will be at least MM:SS
    # so at bare minimum, it is expected that there be only 2 digits on either
    # side of the colon
    # If there are 2 colons, then it is expected that HH is at least 1 digit long,
    # and that MM and SS are still only 2 digits exactly
    # If a radix point is found, then it is expected that there exists either:
    #   - only exists 2 digits before the radix point if a colon was previously found,
    #     no digits may be present after the radix point, in which 0 will be assumed
    #   - if there are no colons present, then there must be at least 1 digit on either 
    #     side of the radix point, in which case the side with no digits will be assumed 
    #     to be 0
    # we should only expect a single instance of the minus sign
    # 
    pass


def print_menu():
    print("1. Encode to WebM")
    print("2. Profiles")
    print("3. Exit")


if __name__ == "__main__":
    user_filename = input("Enter Filename: ")
    file = VideoFileFormat(user_filename)
    file.query_properties()
    print(file)
    print(file.video_stream)
    print(file.audio_stream)
    

    while True:
        # DFC = Duration Find Command
        # CEC = Check Existence Command

        # obtain filename
        if len(sys.argv) > 1:
            user_filename = sys.argv[1]
        else:
            user_filename = input("Enter Filename: ")
            
            
        # Ensure that the filename is a legal name within the Windows File System and
        # strip the extension for future use
        try:
            filename = process_filename_and_extension_strip(user_filename)
        except AssertionError:
            print("Invalid filename")
            continue
        file_info_command = "ffprobe -v error -show_entries format=duration,bit_rate,size  \
                            -show_entries stream=width,height -of default=noprint_wrappers=1 \"{0}\"".format(user_filename)
        try:
            # From "https://www.ffmpeg.org/ffprobe.html":
            #   
            #   "2 Description
            #   ffprobe gathers information from multimedia streams and prints it in human- and machine-readable fashion.
            #   For example it can be used to check the format of the container used by a multimedia stream and the format
            #   and type of each media stream contained in it. If a url is specified in input, ffprobe will try to open and 
            #   probe the url content. If the url cannot be opened or recognized as a multimedia file, a positive exit code is returned."
            # 
            # Simply check for a non-zero return code to determine an invalid file url.
            print((subprocess.check_output(file_info_command, shell=True)).decode("utf-8"))
        except subprocess.CalledProcessError:
            print("Error - File could not be opened")
            continue


        # # Check if the file exists
        # CEC = "IF EXIST \"{0}\" (ECHO True) ELSE (ECHO False)".format(user_filename)
        # try:
        #     file_exists = ((subprocess.check_output(CEC, shell=True)).decode("utf-8")).strip()
        #     if file_exists == "False":
        #         print("File could not be found")
        #         continue          
        # except subprocess.CalledProcessError:
        #    print("Error")
        #    continue
       

        # File clipping
        # user_start_time = input("Enter the starting time: ")
        # user_end_time = input("Enter either the duration or the ending time: ")



        # Select Video/Audio encoding (VP8/VP9 & Vorbis/Opus)
        video_encoding = "libvpx"
        if ((input("Select Video Codec (VP8/VP9): ")).lower() == "vp9"):
            video_encoding += "-vp9"
        audio_encoding = "lib" + (input("Select Audio Codec (Vorbis/Opus): ")).lower()

        # Obtain duration of file in seconds
        DFC = "ffprobe -v error -show_entries format=duration -of default=noprint_wrappers=1:nokey=1 \"{0}\"".format(user_filename)
        try:
            # Capture the output from the DFC command and store it in memory
            returned_duration = (subprocess.check_output(DFC, shell=True)).decode("utf-8") 
            total_time = parse_duration(returned_duration)
        except subprocess.CalledProcessError:
            print("Invalid Data Type")
            continue       
        
        # determine tile_columns & threads from source file
        tile_columns = set_tile_columns(user_filename)
        threads = set_threads(tile_columns)

        # obtain and process input filesize
        user_target_file_size = input("Enter target file size:")
        try:
            target_file_size = process_bitrate(user_target_file_size)
        except AssertionError:
            print("Invalid input")
            continue


        audio_bitrate = process_bitrate(input("Enter audio bitrate: "))


        # determine video_bitrate
        # size in kilobits
        # Max number of kilobits that can be distributed between the streams
        _MAXSIZE = (((target_file_size * 1024 * 1024) * 8) / 1024)
        video_bitrate = ((_MAXSIZE / total_time) * 0.985) - audio_bitrate

        # Ensure that negative values aren't passed onto ffmpeg
        while (video_bitrate <= 0 or audio_bitrate <= 0):
            print("video_bitrate: {0}\naudio_bitrate: {1}\n_MAXSIZE: {2}\nTotal bitrate to work with: {3}".format(video_bitrate, audio_bitrate, _MAXSIZE, (_MAXSIZE / total_time)))
            video_bitrate = input("Enter video_bitrate: ")
            audio_bitrate = input("Enter audio_bitrate: ")

        # obtain and process resolution
        user_dimensions = input("Enter dimensions(Length:Height):")
        dimensions      = process_resolution(user_dimensions)

        # TODO put into VideoFileFormat class
        # First Pass Encoding
        FPE = "ffmpeg -i \"{0}\" -error-resilient 1 -an -c:v {1} -vf unsharp,scale={2} -sws_flags bicubic -quality good -speed 1 -lag-in-frames 25 " \
              "-auto-alt-ref 1 -frame-parallel 0 -tile-columns {3} -g 10 -threads {4} -b:v {5}k -fs {6}MB -pix_fmt yuv420p -c:a {7} -b:a {8}k -ac 2 -f webm -pass 1 -y " \
              "NUL ^".format(user_filename, video_encoding, dimensions, tile_columns, threads, video_bitrate, user_target_file_size, audio_encoding, audio_bitrate)

        # Second Pass Encoding
        SPE = "ffmpeg -i \"{0}\" -error-resilient 1 -c:v {1} -vf unsharp,scale={2} -sws_flags bicubic -quality good -speed 1 -lag-in-frames 25 -auto-alt-ref " \
              "1 -frame-parallel 0 -tile-columns {3} -g 10 -threads {4} -b:v {5}k -fs {6}MB -pix_fmt yuv420p -c:a {7} -b:a {8}k -ac 2 -f webm -pass 2 \"{9}\".webm"\
              .format(user_filename, video_encoding, dimensions, tile_columns, threads, video_bitrate, user_target_file_size, audio_encoding, audio_bitrate, filename)
								
        print("Video Conversion Values:\nvideo_bitrate: {0}\naudio_bitrate: {1}\ntarget_file_size: {2}\ndimensions: {3}".format(video_bitrate, audio_bitrate, target_file_size, dimensions))	
        time.sleep(5)									                                                                                      
																		                                                                                      
        subprocess.run(FPE, shell=True)
        subprocess.run(SPE, shell=True)
        os.remove("ffmpeg2pass-0.log")

        user_view = input("Would you like to view the original video and compare it to the converted video? (y/n): ")
        if user_view == 'y':
            print("yes")
            subprocess.run("mpv \"{0}.webm\" & mpv \"{1}\"".format(filename, user_filename), shell=True)