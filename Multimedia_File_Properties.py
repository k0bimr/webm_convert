import os
import re
import subprocess
from enum import Enum
from typing import Union, List

BYTE_TO_BIT = 8

UNDEFINED_NUM = -1
UNDEFINED_STR = ""

TARGET_FORMAT = "webm"

# TODO Where should these be placed?
WEBM_VIDEO_FORMATS = ["vp9", "vp8", "av1"]
WEBM_AUDIO_FORMATS = ["opus", "vorbis"]

common_lib_prepend = "lib"


class EncodingSetting(object):
    def __init__(self, name: str, data_type: type, value: Union[int, float, str]) -> None:
        self.name = str(name)
        self.type = data_type
        self.value = value

    def get_name(self) -> str:
        return self.name

    def get_data_type(self) -> type:
        return self.type

    def get_value(self) -> Union[int, float, str]:
        return self.value

    def set_value(self, value) -> None:
        self.value = self.get_data_type()(value)

    def __eq__(self, other):
        return self.name == other.name


class FileFormatSetting(EncodingSetting):
    def __init__(self, value: str):
        super(FileFormatSetting, self).__init__(EncodingSettingLabels.TARGET_CONTAINER_FORMAT.value, str, value)


class FileSizeSetting(EncodingSetting):
    def __init__(self, value: int) -> None:
        super(FileSizeSetting, self).__init__(EncodingSettingLabels.TARGET_SIZE.value, int, value)

    def set_value(self, value: str) -> None:
        # Check if the input is just numbers - will be interpreted as bits if so
        if value.isalnum():
            self.value = value
            return

        # Get magnitude
        match = re.search(re.compile("[0-9]*"), value)
        magnitude = self.get_data_type()(value[match.start():match.end()])

        # Get unit
        unit = value[match.end():].strip().upper()  # type: str

        # Do unit conversion - saving as just bits
        try:
            self.value = self.get_data_type()((StorageUnits[unit].value * magnitude) * BYTE_TO_BIT)
        except KeyError:
            print("Specified unit is not recognized")


class VideoFormatSetting(EncodingSetting):
    def __init__(self, value: str) -> None:
        super(VideoFormatSetting, self).__init__(EncodingSettingLabels.TARGET_VIDEO_FORMAT.value, str, value)

    def set_value(self, value: str) -> None:
        if value in WEBM_VIDEO_FORMATS:
            # Video codec: webm accepts vp8, vp9, and av1. vp9 and vp8 are prepended with 'libvpx' while
            #              av1 is prepended with 'libaom'.
            # TODO would like to setup mappings between human-friendly and ffmpeg-friendly codec names
            codec_name = ""
            if "av1" in value.lower():
                codec_name = "aom-"
            elif "vp9" in value.lower():
                codec_name = "vpx-"

            self.value = self.get_data_type()("{0}{1}{2}".format(common_lib_prepend, codec_name, value))


class AudioFormatSetting(EncodingSetting):
    def __init__(self, value: str) -> None:
        super(AudioFormatSetting, self).__init__("TARGET_AUDIO_FORMAT", str, value)
        self.value = value

    def set_value(self, value: str) -> None:
        if value in WEBM_AUDIO_FORMATS:
            self.value = common_lib_prepend + value


class MultimediaFileProperties(Enum):
    DURATION = "duration"
    BIT_RATE = "bit_rate"
    SIZE = "size"
    FORMAT_NAME = "format_name"
    CODEC_NAME = "codec_name"

    WIDTH = "width"
    HEIGHT = "height"

    SAMPLE_RATE = "sample_rate"


# !Duplication of setting name with Enum builtin and EncodingSetting instance variable
class EncodingSettingLabels(Enum):
    TARGET_SIZE = "TARGET_SIZE"
    TARGET_CONTAINER_FORMAT = "TARGET_CONTAINER_FORMAT"
    TARGET_VIDEO_FORMAT = "TARGET_VIDEO_FORMAT"
    TARGET_AUDIO_FORMAT = "TARGET_AUDIO_FORMAT"
    TARGET_COMPRESSION_RATIO = "TARGET_COMPRESSION_RATIO"


class StorageUnits(Enum):
    B = 1
    KB = 1000
    MB = 1000000
    GB = 1000000000


class VideoStreamFormats(Enum):
    AV1 = "av1"
    VP9 = "vp9"
    VP8 = "vp8"


class AudioStreamFormats(Enum):
    OPUS = "opus"
    VORBIS = "vorbis"


def expand_list_into_string(value_list: List[MultimediaFileProperties]) -> str:
    """
    Takes a list and places its values in a comma-separated string.

    :type value_list: MultimediaFileProperties[]
    :param value_list: list of values to be inserted into a string.
    :return: a comma-separated list in the form of a string containing
             the values in value_list.
    """
    comma_separated_values = ""
    for index, value in enumerate(value_list):
        comma_separated_values += value.value
        if index != (len(value_list) - 1):
            comma_separated_values += ","

    return comma_separated_values


def process_filename_and_extension_strip(filename):
    # incoming filename could be any string, but perhaps not one containing
    # illegal characters in the windows filesystem. {/, \, ?, :, *, ", <, >, |}

    illegal_characters = ['/', '\\', '?', ':', '*', '"', '<', '>', '|']

    assert filename != "", "No filename was inputted"
    for current_char in filename:
        # assert current_char not in illegal_characters, "Illegal filename inputted"
        for illegal_char in illegal_characters:
            assert current_char != illegal_char, "Illegal filename inputted"

    PO = ""
    RI = 0  # Radix Index
    FRI = 0  # First Radix Index
    while (True):
        RI = filename[FRI:].find(".")
        if RI != -1:
            FRI = (FRI + RI) + 1
        else:
            FRI -= 1
            break

    if ".webm" in filename:
        PO = "1"
    PF = "{0}{1}".format(filename[:FRI], PO)

    return PF


class VideoFileFormat(object):
    """	docstring for Multimedia_File
        requires ffprobe and ffmpeg

    """

    # COMMON_FLAGS = {
    # 	"-v" 			: "error",
    # 	"-of" 			: "default=noprint_wrappers=1",
    # 	"show_entries"	: None
    # }

    # VIDEO_STREAM_FLAGS = {
    # 	"select_streams" : "v:0",
    # }

    # AUDIO_STREAM_FLAGS = {
    # 	"select_streams" : "a:0"
    # }

    # TODO use the defined constants instead for codec/format names
    PROFILE_SETTINGS = {
        EncodingSettingLabels.TARGET_SIZE: FileSizeSetting(64000000),  # 8 MB in bits
        EncodingSettingLabels.TARGET_CONTAINER_FORMAT: FileFormatSetting(TARGET_FORMAT),
        EncodingSettingLabels.TARGET_VIDEO_FORMAT: VideoFormatSetting("libaom-av1"),
        EncodingSettingLabels.TARGET_AUDIO_FORMAT: AudioFormatSetting("libopus"),
        EncodingSettingLabels.TARGET_COMPRESSION_RATIO: EncodingSetting(
            EncodingSettingLabels.TARGET_COMPRESSION_RATIO.value,
            float, 0.8)
    }

    FILE_FORMAT_PROPERTIES = [
        MultimediaFileProperties.DURATION,
        MultimediaFileProperties.BIT_RATE,
        MultimediaFileProperties.SIZE,
        MultimediaFileProperties.FORMAT_NAME
    ]

    FILE_FORMAT_QUERY = "ffprobe -v error -show_entries format={0} \
	        		-of default=noprint_wrappers=1".format(expand_list_into_string(FILE_FORMAT_PROPERTIES))

    # Default profile filename
    PROFILE_FILENAME = "Profile1.txt"

    def __init__(self, file_name: str) -> None:

        # Dictionary containing file properties
        # self.properties = {
        # 	"video_stream" 	: {},
        # 	"audio_stream" 	: {},
        # 	"file_format" 	: {}
        # }

        # General file attributes
        self.file_name = file_name
        self.file_size = -1
        self.file_duration = -1
        self.file_bit_rate = -1
        self.file_container = ""

        self.video_stream = VideoStreamFormat()
        self.audio_stream = AudioStreamFormat()

    def set_properties(self) -> None:
        # Query properties from ffprobe
        self.query_properties()

        # Check for incomplete data
        if self.file_bit_rate == -1:
            # Do an estimation/average
            if self.file_duration != 0:
                self.file_bit_rate = self.file_size / self.file_duration

        self.audio_stream.query_properties(self.file_name)
        self.video_stream.query_properties(self.file_name)

    def query_properties(self) -> None:
        """
        Queries ffprobe to retrieve information on the file.
        :return:
        """
        try:
            file_format_info = (
                subprocess.check_output(
                    "{0} \"{1}\"".format(VideoFileFormat.FILE_FORMAT_QUERY,
                                         self.file_name),
                    shell=True)).decode("utf-8")
        except subprocess.CalledProcessError:
            print("An error occurred while querying the file info")
            return

        file_format_info = file_format_info.split(os.linesep)
        if (file_format_info[len(file_format_info) - 1] == ""):
            file_format_info.pop()

        # Indexes for the attr_value array or list
        attr_index = 0
        val_index = 1
        for attribute in file_format_info:
            attr_value = attribute.split("=")
            if (attr_value[val_index] != "N/A"):
                if attr_value[attr_index] == MultimediaFileProperties.DURATION.value:
                    self.file_duration = float(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.BIT_RATE.value:
                    self.file_bit_rate = int(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.SIZE.value:
                    self.file_size = int(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.FORMAT_NAME.value:
                    self.file_container = str(attr_value[val_index])

        self.video_stream.query_properties(self.file_name)
        self.audio_stream.query_properties(self.file_name)

    def set_profile_filename(self, profile_filename: str) -> None:
        VideoFileFormat.PROFILE_FILENAME = profile_filename

    def load_profile_settings(self) -> None:
        with open(self.PROFILE_FILENAME) as profile_settings:
            for line in profile_settings:
                if line or not line.isspace():
                    setting = line.split("=")
                    setting_name = setting[0]
                    setting_value = setting[1].strip()

                    try:
                        setting_enum = EncodingSettingLabels[setting_name]  # type: str
                        VideoFileFormat.PROFILE_SETTINGS[setting_enum].set_value(setting_value)
                    except KeyError:
                        print("{0} Could not be matched to a recognized profile setting".format(setting_name))

    def encode(self) -> None:
        # Calculate total bitrate to work with
        # TODO Edge-case: consider file_duration = 0
        target_size = (VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_SIZE]).get_value()
        total_target_bitrate = target_size / self.file_duration

        # Do some data completion
        # TODO edge-case: consider case where total_target_bitrate < DEFAULT_AUDIO_BITRATE_OPUS
        if self.audio_stream.audio_bit_rate == UNDEFINED_NUM:
            audio_bitrate = AudioStreamFormat.DEFAULT_AUDIO_BITRATE_OPUS
        else:
            audio_bitrate = self.audio_stream.audio_bit_rate

        if self.video_stream.video_bit_rate == UNDEFINED_NUM:
            video_bitrate = total_target_bitrate - self.audio_stream.audio_bit_rate
        else:
            video_bitrate = self.video_stream.video_bit_rate

        # Check to see if the bitrates need to be reduced
        if (video_bitrate + audio_bitrate) > total_target_bitrate:
            diff = total_target_bitrate - self.file_bit_rate
            # How will the mins be determined?
            if diff < 0:
                pass

        cmd = "ffmpeg"
        input = "-i \"{}\"".format(self.file_name)
        parameters = "-c:v {0} -b:v {1} -c:a {2} -b:a {3} -f {4} -pass 1 -y".format(
            VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_VIDEO_FORMAT].get_value(),
            video_bitrate,
            VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_AUDIO_FORMAT].get_value(),
            audio_bitrate,
            TARGET_FORMAT
        )
        if "av1" in VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_VIDEO_FORMAT].get_value().lower():
            parameters += " -strict experimental"

        output = "NUL ^"

        first_pass_encode = "{0} {1} {2} {3}".format(cmd, input, parameters, output)

        parameters = "-c:v {0} -b:v {1} -c:a {2} -b:a {3} -f {4} -pass 2 ".format(
            VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_VIDEO_FORMAT].get_value(),
            video_bitrate,
            VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_AUDIO_FORMAT].get_value(),
            audio_bitrate,
            TARGET_FORMAT,
        )
        if "av1" in VideoFileFormat.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_VIDEO_FORMAT].get_value().lower():
            parameters += " -strict experimental"

        output = "\"{0}.{1}\"".format(process_filename_and_extension_strip(self.file_name),
                                      TARGET_FORMAT)

        second_pass_encode = "{0} {1} {2} {3}".format(cmd, input, parameters, output)

        print(first_pass_encode)
        print(second_pass_encode)

        try:
            subprocess.run(first_pass_encode, shell=True)
            subprocess.run(second_pass_encode, shell=True)
            os.remove("ffmpeg2pass-0.log")
        except subprocess.CalledProcessError as process_error:
            print(process_error)

    def __str__(self) -> str:
        return "Filename:  {0}\n" \
               "Size:      {1}\n" \
               "Bitrate:   {2}\n" \
               "Duration:  {3}\n" \
               "Container: {4}\n" \
               "Encoding Settings:\n{5}".format(self.file_name,
                                                self.file_size,
                                                self.file_bit_rate,
                                                self.file_duration,
                                                self.file_container,
                                                self.PROFILE_SETTINGS) + \
               "\n" + \
               self.video_stream.__str__() + \
               "\n" + \
               self.audio_stream.__str__()

    def __eq__(self, other) -> bool:
        return \
            self.file_name == other.file_name and \
            self.file_size == other.file_size and \
            self.file_bit_rate == other.file_bit_rate and \
            self.file_duration == other.file_duration and \
            self.file_container == other.file_container and \
            self.video_stream == other.video_stream and \
            self.audio_stream == other.audio_stream and \
            self.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_SIZE] == other.PROFILE_SETTINGS[
                EncodingSettingLabels.TARGET_SIZE] and \
            self.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_CONTAINER_FORMAT] == other.PROFILE_SETTINGS[
                EncodingSettingLabels.TARGET_CONTAINER_FORMAT] and \
            self.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_VIDEO_FORMAT] == other.PROFILE_SETTINGS[
                EncodingSettingLabels.TARGET_VIDEO_FORMAT] and \
            self.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_AUDIO_FORMAT] == other.PROFILE_SETTINGS[
                EncodingSettingLabels.TARGET_AUDIO_FORMAT] and \
            self.PROFILE_SETTINGS[EncodingSettingLabels.TARGET_COMPRESSION_RATIO] == other.PROFILE_SETTINGS[
                EncodingSettingLabels.TARGET_COMPRESSION_RATIO]


class VideoStreamFormat(object):
    VIDEO_STREAM_PROPERTIES = [
        MultimediaFileProperties.WIDTH,
        MultimediaFileProperties.HEIGHT,
        MultimediaFileProperties.BIT_RATE,
        MultimediaFileProperties.CODEC_NAME,
        MultimediaFileProperties.DURATION
    ]

    VIDEO_STREAM_QUERY = "ffprobe -v error -select_streams v:0 -show_entries \
					    stream=width,height,bit_rate,codec_name:stream_tags=DURATION -of \
					    default=noprint_wrappers=1"

    def __init__(self) -> None:
        self.frame_width = -1
        self.frame_height = -1
        self.video_bit_rate = -1
        self.video_codec_name = ""
        self.video_duration = -1

    def query_properties(self, video_file_name: str) -> None:
        """
        Queries ffprobe to retrieve information on the file.
        :return:
        """
        file_format_info = (
            subprocess.check_output(
                "{0} \"{1}\"".format(VideoStreamFormat.VIDEO_STREAM_QUERY,
                                     video_file_name),
                shell=True)).decode("utf-8")

        file_format_info = file_format_info.split(os.linesep)
        if (file_format_info[len(file_format_info) - 1] == ""):
            file_format_info.pop()

        # Indexes for the attr_value array or list
        attr_index = 0
        val_index = 1
        for attribute in file_format_info:
            attr_value = attribute.split("=")
            if (attr_value[val_index] != "N/A"):
                if attr_value[attr_index] == MultimediaFileProperties.DURATION.value:
                    self.video_duration = float(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.BIT_RATE.value:
                    self.video_bit_rate = int(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.WIDTH.value:
                    self.frame_width = int(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.CODEC_NAME.value:
                    self.video_codec_name = str(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.HEIGHT.value:
                    self.frame_height = int(attr_value[val_index])

    def __str__(self) -> str:
        return "Video Stream Format:\n" \
               "Codec:    {0}\n" \
               "Bitrate:  {1}\n" \
               "Height:   {2}\n" \
               "Width:    {3}\n" \
               "Duration: {4}\n".format(self.video_codec_name,
                                        self.video_bit_rate,
                                        self.frame_height,
                                        self.frame_width,
                                        self.video_duration)

    def __eq__(self, other):
        return self.video_codec_name == other.video_codec_name and \
               self.video_bit_rate == other.video_bit_rate and \
               self.frame_height == other.frame_height and \
               self.frame_width == other.frame_width and \
               self.video_duration == other.video_duration


class AudioStreamFormat(object):
    DEFAULT_AUDIO_BITRATE_OPUS = 64000

    AUDIO_STREAM_PROPERTIES = [
        MultimediaFileProperties.BIT_RATE,
        MultimediaFileProperties.CODEC_NAME,
        MultimediaFileProperties.SAMPLE_RATE,
        MultimediaFileProperties.DURATION
    ]

    AUDIO_STREAM_QUERY = "ffprobe -v error -select_streams a:0 -show_entries \
					    stream=sample_rate,bit_rate,codec_name:stream_tags=DURATION -of \
					    default=noprint_wrappers=1"

    def __init__(self) -> None:
        self.audio_bit_rate = -1
        self.audio_codec_name = ""
        self.audio_sample_rate = -1
        self.audio_duration = -1

    def query_properties(self, video_file_name: str) -> None:
        """
        Queries ffprobe to retrieve information on the file.
        :return:
        """
        file_format_info = (
            subprocess.check_output(
                "{0} \"{1}\"".format(AudioStreamFormat.AUDIO_STREAM_QUERY,
                                     video_file_name),
                shell=True)).decode("utf-8")

        file_format_info = file_format_info.split(os.linesep)
        if (file_format_info[len(file_format_info) - 1] == ""):
            file_format_info.pop()

        # Indexes for the attr_value array or list
        attr_index = 0
        val_index = 1
        for attribute in file_format_info:
            attr_value = attribute.split("=")
            if (attr_value[val_index] != "N/A"):
                if attr_value[attr_index] == MultimediaFileProperties.DURATION.value:
                    self.audio_duration = float(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.BIT_RATE.value:
                    self.audio_bit_rate = int(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.CODEC_NAME.value:
                    self.audio_codec_name = str(attr_value[val_index])
                elif attr_value[attr_index] == MultimediaFileProperties.SAMPLE_RATE.value:
                    self.audio_sample_rate = int(attr_value[val_index])

    def __str__(self) -> str:
        return "Audio Stream Format:\n" \
               "Codec:       {0}\n" \
               "Bit rate:    {1}\n" \
               "Sample Rate: {2}\n" \
               "Duration:    {3}\n".format(self.audio_codec_name,
                                           self.audio_bit_rate,
                                           self.audio_sample_rate,
                                           self.audio_duration)

    def __eq__(self, other):
        return self.audio_codec_name == other.audio_codec_name and \
               self.audio_bit_rate == other.audio_bit_rate and \
               self.audio_sample_rate == other.audio_sample_rate and \
               self.audio_duration == other.audio_duration
