from Multimedia_File_Properties import VideoFileFormat, EncodingSettings
import os

filename = "1485192284.webm"


if __name__ == "__main__":
    # file = VideoFileFormat(filename)
    # file.query_properties()
    #
    # expected_file = VideoFileFormat(filename)
    # expected_file.file_size = 5861665
    # expected_file.file_bit_rate = 619380
    # expected_file.file_duration = 75.710000
    # expected_file.file_container = "matroska,webm"
    #
    # expected_file.video_stream.video_bit_rate = -1
    # expected_file.video_stream.video_codec_name = "vp8"
    # expected_file.video_stream.video_duration = -1
    # expected_file.video_stream.frame_width = 640
    # expected_file.video_stream.frame_height = 360
    #
    # expected_file.audio_stream.audio_bit_rate = -1
    # expected_file.audio_stream.audio_codec_name = "vorbis"
    # expected_file.audio_stream.audio_sample_rate = 44100
    # expected_file.audio_stream.audio_duration = -1
    #
    # if file == expected_file:
    #     print("Pass")
    # else:
    #     print("Fail")
    #     print("Video: {}".format(file))
    #     print("Expected Video: {}".format(expected_file))



    file = VideoFileFormat("test")
    file.set_profile_filename("profiles\\8mb_lim.txt")
    file.load_profile_settings()

    # expected_file = VideoFileFormat("test")
    # expected_file.EXPECTED_PROFILE_SETTINGS = {
    #     EncodingSettings.TARGET_SIZE : 64000,  # In MB
    #     EncodingSettings.TARGET_CONTAINER_FORMAT : "WebM",
    #     EncodingSettings.TARGET_VIDEO_FORMAT : "vp9",
    #     EncodingSettings.TARGET_AUDIO_FORMAT : "opus",
    #     EncodingSettings.TARGET_COMPRESSION_RATIO : 0.8
    # }

    print(file)
    # if file == expected_file:
    #     print("Read profile settings PASS")
    # else:
    #     print("FAIL")
